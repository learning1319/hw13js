/*1. setTimeout() - запускається лише 1 раз через вказаний інтервал,
     setInterval() - повторюється багато разів, допоки не зупинимо clearrTimeout
2. Функція спрацює тільки після виконання всього коду, отже не миттєво. 
3. clearInterval() потрібен для зупинки роботи функції setInterval(). 
Коли в роботі функції немає потреби, її варто зупинити, аби не навантажувати пам'ять. 
*/

const banners = document.querySelectorAll(".image-to-show");

let i = 0;
let bannersChanger;
let setTimer;

const changeImg = function () {
  banners.forEach((elem) => {
    elem.classList.add("displayNone");
  });
  console.log(i);
  banners[i++].classList.remove("displayNone");

  if (i >= banners.length) {
    i = 0;
  }

  timer(3);

  bannersChanger = setTimeout(changeImg, 3000);
};

changeImg();

function timer(delayValue) {
  document.querySelector(".timer").textContent = delayValue;
  delayValue--;

  if (delayValue < 1) {
    clearTimeout(setTimer);
  } else {
    setTimer = setTimeout(function () {
      timer(delayValue);
    }, 1000);
  }
}

setTimeout(function () {
  const buttons = document.querySelectorAll(".btn");
  buttons.forEach((elem) => {
    elem.classList.remove("displayNone");
  });
}, 3000);

const stopButton = document.getElementById("stopButton");
const resumeButton = document.getElementById("resumeButton");

document
  .querySelector(".btn-wrapper")
  .addEventListener("click", function (event) {
    console.log(event.target);
    if (event.target === stopButton) {
      clearTimeout(bannersChanger);
      clearTimeout(setTimer);
    }
    if (event.target === resumeButton) {
      changeImg();
    }
  });
